namespace Model.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class vietvangdbcontext : DbContext
    {
        public vietvangdbcontext()
            : base("name=vietvangdbcontext")
        {
        }

        public virtual DbSet<table_demo1> table_demo1 { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<table_demo1>()
                .Property(e => e.ID)
                .IsFixedLength();

            modelBuilder.Entity<table_demo1>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<table_demo1>()
                .Property(e => e.Age)
                .IsFixedLength();
        }
    }
}
