﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;
namespace TodoApi.Data
{
    public class DbInitializer
    {
        public static void Initialize(TodoApiContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.TodoItems.Any())
            {
                return;   // DB has been seeded
            }

            var TodoItems = new TodoItem[]
            {
                new TodoItem{Name="Carson",IsComplete=true},
                };
            foreach (TodoItem s in TodoItems)
            {
                context.TodoItems.Add(s);
            }
            context.SaveChanges();


        }
    }
}
