﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Core_Web.Models;
using MySql.Data.MySqlClient;
namespace Core_Web.Controllers
{
    [Route("coreweb/[controller]")]
    [ApiController]
    public class AlbumsController : Controller

    {
        private readonly MusicStoreContext _context;

        public AlbumsController(MusicStoreContext context)
        {
            _context = context;
        }
        /* public IActionResult Index()
         {
             MusicStoreContext context = HttpContext.RequestServices.GetService(typeof(Core_Web.Models.MusicStoreContext)) as MusicStoreContext;

             return View(context.GetAllAlbums());
         }*/

        [HttpGet]
        public List<Album> GetAllAlbums()
        {
            List<Album> list = new List<Album>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from Album", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Album()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = reader["Name"].ToString(),
                            ArtistName = reader["ArtistName"].ToString(),
                            Price = Convert.ToInt32(reader["Price"]),
                            Genre = reader["genre"].ToString()
                        });
                    }
                }
                conn.Close();
            }
            return list;
        }
        [HttpGet("{id}")]
        public List<Album> GetAllAlbumsByID(int id)
        {
            List<Album> list = new List<Album>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from Album where id= " + id, conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Album()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = reader["Name"].ToString(),
                            ArtistName = reader["ArtistName"].ToString(),
                            Price = Convert.ToInt32(reader["Price"]),
                            Genre = reader["genre"].ToString()
                        });
                    }
                }
                conn.Close();
            }
            return list;
        }
        [HttpPost]
        public List<Album> PostAlbum(Album album)
        {
            List<Album> list = new List<Album>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String insertAlbum = "insert into album (Name, ArtistName, Price, genre) values (@name, @artistname, @price, @genre)";
                MySqlCommand cmd = new MySqlCommand(insertAlbum, conn);
                cmd.Parameters.AddWithValue("@name", album.Name);
                cmd.Parameters.AddWithValue("@artistName", album.ArtistName);
                cmd.Parameters.AddWithValue("@price", album.Price);
                cmd.Parameters.AddWithValue("@genre", album.Genre);
                cmd.ExecuteNonQuery();
                String returnLastValue = "SELECT * FROM album ORDER BY ID DESC LIMIT 1";
                MySqlCommand cmdreturnLastValue = new MySqlCommand(returnLastValue, conn);
                using (var reader = cmdreturnLastValue.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Album()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = reader["Name"].ToString(),
                            ArtistName = reader["ArtistName"].ToString(),
                            Price = Convert.ToInt32(reader["Price"]),
                            Genre = reader["genre"].ToString()
                        });
                    }
                }
                conn.Close();
            }

            return list;
        }

        [HttpPut("{id}")]
        public List<Album> EditAlbum(long id, Album album)
        {
            List<Album> list = new List<Album>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String CheckID = "SELECT 1 FROM album WHERE id = @id";
                MySqlCommand cmdCheckID = new MySqlCommand(CheckID, conn);
                cmdCheckID.Parameters.AddWithValue("@id", id);
                using (var reader1 = cmdCheckID.ExecuteReader())
                {
                    if (reader1 == null)
                    {
                        return null;
                    }
                    conn.Close();
                    conn.Open();
                    String editAlbum = "Update album SET Name=@name, ArtistName=@artistName, Price=@price, genre=@genre where id=@id";
                    MySqlCommand cmdEditAlbum = new MySqlCommand(editAlbum, conn);
                    cmdEditAlbum.Parameters.AddWithValue("@name", album.Name);
                    cmdEditAlbum.Parameters.AddWithValue("@artistName", album.ArtistName);
                    cmdEditAlbum.Parameters.AddWithValue("@price", album.Price);
                    cmdEditAlbum.Parameters.AddWithValue("@genre", album.Genre);
                    cmdEditAlbum.Parameters.AddWithValue("@id", id);
                    cmdEditAlbum.ExecuteNonQuery();
                    String returnLastValue = "SELECT * FROM album where id= @id";
                    MySqlCommand cmdreturnEditValue = new MySqlCommand(returnLastValue, conn);
                    cmdreturnEditValue.Parameters.AddWithValue("@id", id);
                    using (var reader = cmdreturnEditValue.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Album()
                            {
                                Id = Convert.ToInt32(reader["Id"]),
                                Name = reader["Name"].ToString(),
                                ArtistName = reader["ArtistName"].ToString(),
                                Price = Convert.ToInt32(reader["Price"]),
                                Genre = reader["genre"].ToString()
                            });
                        }
                    }
                }
                conn.Close();
            }
            return list;
        }

        [HttpDelete("{id}")]
        public List<Album> DeleteAlbum(long id)
        {
            List<Album> list = new List<Album>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM album WHERE id=@id", conn);
                cmd.Parameters.AddWithValue("@id", id);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Album()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = reader["Name"].ToString(),
                            ArtistName = reader["ArtistName"].ToString(),
                            Price = Convert.ToInt32(reader["Price"]),
                            Genre = reader["genre"].ToString()
                        });
                    }
                }
                conn.Close();
            }
            return list;
        }
        public IActionResult Index()

        {
            MusicStoreContext context = HttpContext.RequestServices.GetService(typeof(Core_Web.Models.MusicStoreContext)) as MusicStoreContext;

            return View(GetAllAlbums());
        }
    }
}