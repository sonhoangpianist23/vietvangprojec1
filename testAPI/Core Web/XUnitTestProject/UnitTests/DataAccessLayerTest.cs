﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Core_Web.Controllers;
using Core_Web.Models;

namespace XUnitTestProject.UnitTests
{
    public class DataAccessLayerTest
    {
         AlbumsController albumsController = new AlbumsController(Utilities.Utilities.musicStoreContext);

        [Fact]
        public void TestGetAll()
        {
            var result = albumsController.GetAllAlbums();
            Assert.Equal(7, result.Count);
        }
        [Fact]
        public void TestGetByID()
        {
            var result = albumsController.GetAllAlbumsByID(2);
            Assert.Single(result);
        }
        [Fact]
        public void TestPostAlbum()
        {
            var result = albumsController.PostAlbum(album);
            Assert.Single(result);
        }
        [Fact]
        public void TestEditAlbum()
        {
            var result = albumsController.EditAlbum(2, album);
            Assert.Single(result);
        }
        [Fact]
        public void TestDeleteAlbum()
        {
            var result = albumsController.DeleteAlbum(2);
            Assert.Empty(result);
        }

        Album album = new Album() {Name="test", ArtistName="arttest",Price=10, Genre="testgenre" };
        


    }
}
