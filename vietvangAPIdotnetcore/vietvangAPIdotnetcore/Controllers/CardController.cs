﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using vietvangAPIdotnetcore.Models.EF;
using vietvangAPIdotnetcore.Models;
using MySql.Data.MySqlClient;

namespace vietvangAPIdotnetcore.Controllers
{
    [Route("vietvang/[controller]")]
    [ApiController]
    public class CardController : Controller
    {
        private readonly VietVangApiContext _context;
        public CardController(VietVangApiContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get all of Card.
        /// </summary>
        /// <response code="201">Returns all of areacode Items</response>
        /// <response code="400">If the item is null</response>  
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public List<Card> GetAllCard()
        {
            List<Card> list = new List<Card>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM cardmanagement WHERE status = 0", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Card()
                        {
                            cardID = Convert.ToInt32(reader["cardID"]),
                            cardName = reader["cardName"].ToString(),
                            cardImage = reader["cardImage"].ToString(),
                            status = Convert.ToInt32(reader["status"]),
                        });
                    }
                }
                conn.Close();
            }
            return list;
        }
        /// <summary>
        /// GET specific Card by unique cardid.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET/GetCardByID/1
        /// </remarks>
        [HttpGet("GetCardByID")]
        public List<Card> GetCardByID(int cardid)
        {
            List<Card> list = new List<Card>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from cardmanagement where cardId= @ID ", conn);
                cmd.Parameters.AddWithValue("@ID", cardid);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Card()
                        {
                            cardID = Convert.ToInt32(reader["cardID"]),
                            cardName = reader["cardName"].ToString(),
                            cardImage = reader["cardImage"].ToString(),
                            status = Convert.ToInt32(reader["status"]),
                        });
                    }
                }
                conn.Close();
            }
            return list;
        }
        /// <summary>
        /// POST specific Card by card obj.
        /// </summary>
        /// <remarks>
        /// {
        /// "cardName": "cardtest",
        /// "cardImage": "cardtest.img",
        /// "status": 0
        ///}
        /// </remarks>
        [HttpPost]
        public Boolean AddCard(Card card)
        {
            List<Card> list = new List<Card>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String insertCard = "INSERT INTO cardmanagement (cardName,cardImage,status) VALUES (@cardName,@cardImage,@status)";
                MySqlCommand cmd = new MySqlCommand(insertCard, conn);
                cmd.Parameters.AddWithValue("@cardName", card.cardName);
                cmd.Parameters.AddWithValue("@cardImage", card.cardImage);
                cmd.Parameters.AddWithValue("@status", 0);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                conn.Close();
            }
        }

        /// <summary>
        /// UPDATE specific Card by Card obj.
        /// </summary>
        /// <remarks>
        /// 
        /// UPDATE/UpdateCard?cardID=9
        /// {
        /// "cardName": "cardtest",
        /// "cardImage": "cardtest.img",
        /// "status": 0
        ///}
        /// </remarks>

        [HttpPut("UpdateCard")]
        public bool UpdateCard(int cardID, Card card)
        {
            List<Card> list = new List<Card>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String UpdateCard = "Update cardmanagement SET cardName = @cardName , cardImage = @cardImage, status=@status WHERE cardId = @cardID";
                MySqlCommand cmd = new MySqlCommand(UpdateCard, conn);
                cmd.Parameters.AddWithValue("@cardID", cardID);
                cmd.Parameters.AddWithValue("@cardName", card.cardName);
                cmd.Parameters.AddWithValue("@cardImage", card.cardImage);
                cmd.Parameters.AddWithValue("@status", card.status);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                conn.Close();
            }
        }

        /// <summary>
        /// DELETE card by change "status" into 1 
        /// </summary>
        /// <param name="cardID"></param>
        /// <returns></returns>
        ///    /// <remarks>
        /// 
        /// UPDATE/DeleteCard?cardID=9
        /// </remarks>
        [HttpPut("DeleteCard")]
        public bool DeleteCard(int cardID)
        {
            List<Card> list = new List<Card>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String DeleteCard = "UPDATE cardmanagement SET status = 1 WHERE cardId = @cardID";
                MySqlCommand cmd = new MySqlCommand(DeleteCard, conn);
                cmd.Parameters.AddWithValue("@cardID", cardID);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                conn.Close();
            }
        }
    }
}