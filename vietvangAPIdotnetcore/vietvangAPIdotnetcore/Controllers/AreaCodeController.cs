﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using vietvangAPIdotnetcore.Models;
using vietvangAPIdotnetcore.Models.EF;
using MySql.Data.MySqlClient;
using System.Web;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Prometheus;
using BenchmarkDotNet.Reports;
using Microsoft.AspNetCore.Http;

namespace vietvangAPIdotnetcore.Controllers
{
    [Route("vietvang/[controller]")]
    [ApiController]
    public class AreaCodeController : Controller
    {
        private readonly VietVangApiContext _context;
        public AreaCodeController(VietVangApiContext context)
        {
            _context = context;
        }
        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }


        /// <summary>
        /// Get all specific AreaCode.
        /// </summary>
        /// <response code="201">Returns all of areacode Items</response>
        /// <response code="400">If the item is null</response>  
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public List<AreaCode> GetAreaCode()
        {
            List<AreaCode> list = new List<AreaCode>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM areacode", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new AreaCode()
                        {
                            areaCodeID = Convert.ToInt32(reader["areaCodeID"]),
                            areaCode = reader["areaCode"].ToString(),
                            updatedDate = ObjectToByteArray(reader["updatedDate"])
                        });
                    }
                }
                conn.Close();
            }
            return list;
        }


        /// <summary>
        /// Check Have specific AreaCode by unique ID.
        /// </summary>
        /// <param name="areaCode"> code of area</param>
        /// <remarks>
        /// Sample request:
        /// GET/CheckAreaCode/id=1
        /// </remarks>
        [HttpGet("CheckAreaCode")]
        public Boolean CheckAreaCode(int areaCode)
        {
            List<AreaCode> list = new List<AreaCode>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from areacode where areaCode= @ID ", conn);
                cmd.Parameters.AddWithValue("@ID", areaCode);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new AreaCode()
                        {
                            areaCodeID = Convert.ToInt32(reader["areaCodeID"]),
                            areaCode = reader["areaCode"].ToString(),
                            updatedDate = ObjectToByteArray(reader["updatedDate"])
                        });
                    }
                    if (list.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                conn.Close();
            }
        }
        /// <summary>
        /// GET specific PostCode by plz.
        /// </summary>
        /// <param name="plz"> :plz</param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET/GetPostCode/q  
        /// </remarks>
        [HttpGet("GetPostCode")]
        public List<PostCode> GetPostCode(string plz)
        {
            List<PostCode> list = new List<PostCode>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from postcode where postcode.plz= @plz ", conn);
                cmd.Parameters.AddWithValue("@plz", plz);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new PostCode()
                        {
                            postcodeID = Convert.ToInt32(reader["postcodeID"]),
                            plz = reader["plz"].ToString(),
                            ort = reader["ort"].ToString(),
                            areaCode = reader["areaCode"].ToString(),
                            updatedDate = ObjectToByteArray(reader["updatedDate"])
                        });
                    }

                }
                conn.Close();
            }
            return list;
        }
        /// <summary>
        /// POST specific AreaCode by areacode obj.
        /// </summary>
        /// <param name="areaCode"> areacode id </param>
        /// <returns></returns>
        [HttpPost]
        public List<AreaCode> AddAreaCode(String areaCode)
        {
            List<AreaCode> list = new List<AreaCode>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String insertAAreaCode = "INSERT INTO `areacode`(`areaCode`) VALUES(@areaCode)";
                MySqlCommand cmd = new MySqlCommand(insertAAreaCode, conn);
                cmd.Parameters.AddWithValue("@areaCode", areaCode);
                cmd.ExecuteNonQuery();
                String returnLastValue = "SELECT * FROM areacode ORDER BY areaCodeID DESC LIMIT 1";
                MySqlCommand cmdreturnLastValue = new MySqlCommand(returnLastValue, conn);
                using (var reader = cmdreturnLastValue.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new AreaCode()
                        {
                            areaCodeID = Convert.ToInt32(reader["areaCodeID"]),
                            areaCode = reader["areaCode"].ToString(),
                            updatedDate = ObjectToByteArray(reader["updatedDate"])
                        });
                    }
                }
                conn.Close();
            }

            return list;
        }
        /// <summary>
        /// DELETE all data in table.
        /// </summary>

        [HttpDelete("LoadDataInToAreas")]
        public Boolean LoadDataInToAreas()
        {
            // List<Album> list = new List<Album>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("TRUNCATE `areacode`", conn);
                cmd.ExecuteReader();
                conn.Close();
                return true;
            }
        }
    }

}