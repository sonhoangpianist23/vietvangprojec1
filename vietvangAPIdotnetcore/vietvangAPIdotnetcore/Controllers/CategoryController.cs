﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using Google.Protobuf;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using vietvangAPIdotnetcore.Models;
using vietvangAPIdotnetcore.Models.EF;
namespace vietvangAPIdotnetcore.Controllers
{
    [Route("vietvang/[controller]")]
    [ApiController]

    public class CategoryController : Controller
    {

        private readonly VietVangApiContext _context;
        public CategoryController(VietVangApiContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get Category from noStart to noEnd in "position" field.
        /// </summary>
        /// <remarks>
        /// GE/?noStart=0&noEnd=4
        /// </remarks>
       
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public List<Category> GetCategory(int noStart, int noEnd)
        {
            List<Category> list = new List<Category>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM category ORDER BY `category`.`position` LIMIT @noStart,@noEnd", conn);
                cmd.Parameters.AddWithValue("@noStart", noStart);
                cmd.Parameters.AddWithValue("@noEnd", noEnd);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (reader["parentID"] != DBNull.Value && reader["image"] != DBNull.Value)
                        {
                            list.Add(new Category()
                            {
                                categoryID = Convert.ToInt32(reader["categoryID"]),
                                categoryName = reader["categoryName"].ToString(),
                                parentID = Convert.ToInt32(reader["parentID"]),
                                image = reader["image"].ToString(),
                                color = Convert.ToInt32(reader["color"]),
                                position = Convert.ToInt32(reader["position"]),
                                //  isBuffet = Convert.ToByte(ObjectTostring(reader["isBuffet"])),
                                isBuffet = Convert.ToInt16(reader["isBuffet"]),
                                updatedDate = ObjectToByteArray(reader["updatedDate"])

                            });
                        }
                        if (reader["parentID"] == DBNull.Value && reader["image"] == DBNull.Value)
                        {
                            list.Add(new Category()
                            {
                                categoryID = Convert.ToInt32(reader["categoryID"]),
                                categoryName = reader["categoryName"].ToString(),
                                image = "null",
                                parentID = 0,
                                color = Convert.ToInt32(reader["color"]),
                                position = Convert.ToInt32(reader["position"]),
                                isBuffet = Convert.ToInt16(reader["isBuffet"]),
                                updatedDate = ObjectToByteArray(reader["updatedDate"])

                            });
                        }
                        if (reader["parentID"] == DBNull.Value && reader["image"] != DBNull.Value)
                        {
                            list.Add(new Category()
                            {
                                categoryID = Convert.ToInt32(reader["categoryID"]),
                                categoryName = reader["categoryName"].ToString(),
                                image = reader["image"].ToString(),
                                parentID = 0,
                                color = Convert.ToInt32(reader["color"]),
                                position = Convert.ToInt32(reader["position"]),
                                //  isBuffet = Convert.ToByte(ObjectTostring(reader["isBuffet"])),
                                isBuffet = Convert.ToInt16(reader["isBuffet"]),
                                updatedDate = ObjectToByteArray(reader["updatedDate"])

                            });
                        }
                        if (reader["parentID"] != DBNull.Value && reader["image"] == DBNull.Value)
                        {
                            list.Add(new Category()
                            {
                                categoryID = Convert.ToInt32(reader["categoryID"]),
                                categoryName = reader["categoryName"].ToString(),
                                parentID = Convert.ToInt32(reader["parentID"]),
                                image = "null",
                                color = Convert.ToInt32(reader["color"]),
                                position = Convert.ToInt32(reader["position"]),
                                //  isBuffet = Convert.ToByte(ObjectTostring(reader["isBuffet"])),
                                isBuffet = Convert.ToInt16(reader["isBuffet"]),
                                updatedDate = ObjectToByteArray(reader["updatedDate"])

                            });
                        }
                    }
                }
                conn.Close();
            }
            return list;
        }

        /// <summary>
        /// Get count of Category item in Category table.
        /// </summary>
     
        [HttpGet("GetTotalCategory")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public int GetTotalCategory()
        {
            List<Category> list = new List<Category>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT COUNT(*) as Total FROM category", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    int count = 0;
                    while (reader.Read())
                    {
                        count = Convert.ToInt32(reader["Total"]);
                    }
                    return count;
                }
            }


        }
        /// <summary>
        /// Get specific CategotryName by categotyID.
        /// </summary>
        /// <remarks>
        /// GET/GetCatNameByCatid?id=12
        /// </remarks>
        /// <response code="201">Returns all of areacode Items</response>
        /// <response code="400">If the item is null</response>  
        [HttpGet("GetCatNameByCatid")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public List<Category> GetCatNameByCatid(int id)
        {
            List<Category> list = new List<Category>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT `categoryName` FROM `category` WHERE `categoryID` = @id", conn);
                cmd.Parameters.AddWithValue("@id", id);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Category()
                        {
                            categoryName = reader["categoryName"].ToString(),
                        });
                    }
                    conn.Close();
                }
                return list;
            }
        }
        /// <summary>
        /// Get all item.
        /// </summary>
        [HttpGet("GetAllCategory")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public List<Category> GetAllCategory()
        {
            List<Category> list = new List<Category>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM category ORDER BY `category`.`position`", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (reader["parentID"] != DBNull.Value && reader["image"] != DBNull.Value)
                        {
                            list.Add(new Category()
                            {
                                categoryID = Convert.ToInt32(reader["categoryID"]),
                                categoryName = reader["categoryName"].ToString(),
                                parentID = Convert.ToInt32(reader["parentID"]),
                                image = reader["image"].ToString(),
                                color = Convert.ToInt32(reader["color"]),
                                position = Convert.ToInt32(reader["position"]),
                                //  isBuffet = Convert.ToByte(ObjectTostring(reader["isBuffet"])),
                                isBuffet = Convert.ToInt16(reader["isBuffet"]),
                                updatedDate = ObjectToByteArray(reader["updatedDate"])

                            });
                        }
                        if (reader["parentID"] == DBNull.Value && reader["image"] == DBNull.Value)
                        {
                            list.Add(new Category()
                            {
                                categoryID = Convert.ToInt32(reader["categoryID"]),
                                categoryName = reader["categoryName"].ToString(),
                                image = "null",
                                parentID = 0,
                                color = Convert.ToInt32(reader["color"]),
                                position = Convert.ToInt32(reader["position"]),
                                isBuffet = Convert.ToInt16(reader["isBuffet"]),
                                updatedDate = ObjectToByteArray(reader["updatedDate"])

                            });
                        }
                        if (reader["parentID"] == DBNull.Value && reader["image"] != DBNull.Value)
                        {
                            list.Add(new Category()
                            {
                                categoryID = Convert.ToInt32(reader["categoryID"]),
                                categoryName = reader["categoryName"].ToString(),
                                image = reader["image"].ToString(),
                                parentID = 0,
                                color = Convert.ToInt32(reader["color"]),
                                position = Convert.ToInt32(reader["position"]),
                                //  isBuffet = Convert.ToByte(ObjectTostring(reader["isBuffet"])),
                                isBuffet = Convert.ToInt16(reader["isBuffet"]),
                                updatedDate = ObjectToByteArray(reader["updatedDate"])

                            });
                        }
                        if (reader["parentID"] != DBNull.Value && reader["image"] == DBNull.Value)
                        {
                            list.Add(new Category()
                            {
                                categoryID = Convert.ToInt32(reader["categoryID"]),
                                categoryName = reader["categoryName"].ToString(),
                                parentID = Convert.ToInt32(reader["parentID"]),
                                image = "null",
                                color = Convert.ToInt32(reader["color"]),
                                position = Convert.ToInt32(reader["position"]),
                                //  isBuffet = Convert.ToByte(ObjectTostring(reader["isBuffet"])),
                                isBuffet = Convert.ToInt16(reader["isBuffet"]),
                                updatedDate = ObjectToByteArray(reader["updatedDate"])

                            });
                        }
                    }
                    conn.Close();
                }
                return list;
            }
        }
        /// <summary>
        /// CheckIsBuffet by categoryID
        /// </summary>
        /// <remarks>
        /// GET/CheckIsBuffet?id=12
        /// </remarks>

        [HttpGet("CheckIsBuffet")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public int CheckIsBuffet(int id)
        {
            List<Category> list = new List<Category>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT isBuffet FROM `category` WHERE `categoryID` = @id", conn);
                cmd.Parameters.AddWithValue("@id", id);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Category()
                        {

                            isBuffet = Convert.ToInt16(reader["isBuffet"]),


                        });
                        //AAEAAAD/////AQAAAAAAAAAEAQAAAAxTeXN0ZW0uU0J5dGUBAAAAB21fdmFsdWUACgEL
                    }
                    conn.Close();

                    return list[0].isBuffet;

                }
            }
        }

        /// <summary>
        /// add more category Item in category field
        /// </summary>
        /// <remarks>
        /// POST?categoryName=dog&color=14&position=14&isBuffet=0
        /// </remarks>
        /// <param name="categoryName"></param>
        /// <param name="color"></param>
        /// <param name="position"></param>
        /// <param name="isBuffet"></param>
        /// <returns></returns>
        [HttpPost]
        public Boolean AddCategory(string categoryName, int color, int position, int isBuffet)
        {
            List<Category> list = new List<Category>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String AddCategory = "INSERT INTO category (categoryName, color, position, isBuffet) VALUES(@categoryName, @color, @position, @isBuffet)";
                MySqlCommand cmd = new MySqlCommand(AddCategory, conn);
                cmd.Parameters.AddWithValue("@categoryName", categoryName);
                cmd.Parameters.AddWithValue("@color", color);
                cmd.Parameters.AddWithValue("@position", position);
                cmd.Parameters.AddWithValue("@isBuffet", isBuffet);
                cmd.ExecuteNonQuery();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                conn.Close();
            }

        }
        /// <summary>
        /// Update Category on Category field
        /// </summary>
        /// <remarks>
        /// PUT/UpdateCategory?id=14&name=cat&position=14&isBuffet=1
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="position"></param>
        /// <param name="isBuffet"></param>
        /// <returns></returns>
        [HttpPut("UpdateCategory")]
        public bool UpdateCategory(int id, string name, int position,int isBuffet)
        {
            List<Category> list = new List<Category>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String UpdateCard = "UPDATE `category` SET `category`.`categoryName` = @name , `category`.`position` = @position , `isBuffet` = @isBuffet WHERE `category`.`categoryID` = @id";
                MySqlCommand cmd = new MySqlCommand(UpdateCard, conn);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@position", position);
                cmd.Parameters.AddWithValue("@isBuffet", isBuffet);
                cmd.Parameters.AddWithValue("@id", id);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                conn.Close();
            }
        }
        /// <summary>
        /// delete all items in Category Field
        /// </summary>
        /// <remarks>
        /// DELETE/DeleteCategory?id=13
        /// </remarks>
        [HttpDelete("DeleteCategory")]
        public bool DeleteCategory(int id)
        {
            List<Category> list = new List<Category>();
            using (MySqlConnection conn = _context.GetConnection())
            {
                conn.Open();
                String DeleteCard = "DELETE FROM `category` WHERE `categoryID` = @id";
                MySqlCommand cmd = new MySqlCommand(DeleteCard, conn);
                cmd.Parameters.AddWithValue("@id", id);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                conn.Close();
            }
        }
        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }


        private string ObjectTostring(Object obj)
        {

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToString();
        }

    }
}
