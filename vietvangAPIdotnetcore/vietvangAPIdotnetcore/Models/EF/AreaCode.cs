﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
namespace vietvangAPIdotnetcore.Models.EF
{
	public class AreaCode
	{
		public int areaCodeID { get; set; }
		public string areaCode { get; set; }
		public byte[] updatedDate { get; set; }

		public AreaCode(int areaCodeID_, string areaCode_, byte[] updatedDate_)
		{
			this.areaCodeID = areaCodeID_;
			this.areaCode = areaCode_;
			this.updatedDate = updatedDate_;
		}
	public AreaCode()
        {
        }
    }
}
