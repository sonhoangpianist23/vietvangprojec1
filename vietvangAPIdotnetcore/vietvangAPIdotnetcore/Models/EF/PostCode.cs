﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vietvangAPIdotnetcore.Models.EF
{
	public class PostCode
	{
		public int postcodeID { get; set; }
		public string plz { get; set; }
		public string ort { get; set; }
		public string areaCode { get; set; }
		public byte[] updatedDate { get; set; }

		public PostCode(int postcodeID_, string plz_, string ort_, string areaCode_, byte[] updatedDate_)
		{
			this.postcodeID = postcodeID_;
			this.plz = plz_;
			this.ort = ort_;
			this.areaCode = areaCode_;
			this.updatedDate = updatedDate_;
		}

		public PostCode()
		{
		}
	}
}
