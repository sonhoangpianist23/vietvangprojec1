﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vietvangAPIdotnetcore.Models.EF
{
    public class Category
    {
		public int categoryID { get; set; }
		public string categoryName { get; set; }
		public string image { get; set; }
		public int parentID { get; set; }
		public int color { get; set; }
		public int position { get; set; }
		public int isBuffet { get; set; }
		public byte[] updatedDate { get; set; }

		public Category(int categoryID_, string categoryName_, String image_, int parentID_, int color_, int position_, int isBuffet_, byte[] updatedDate_)
		{
			this.categoryID = categoryID_;
			this.categoryName = categoryName_;
			this.image = image_;
			this.parentID = parentID_;
			this.color = color_;
			this.position = position_;
			this.isBuffet = isBuffet_;
			this.updatedDate = updatedDate_;
		}

		public Category()
		{
		}
	}
}
