﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vietvangAPIdotnetcore.Models.EF
{
    public class Card
    {
		public int cardID { get; set; }
		public string cardName { get; set; }
		public string cardImage { get; set; }
		public int status { get; set; }

		public Card(int cardID_, string cardName_, string cardImage_, int status_)
		{
			this.cardID = cardID_;
			this.cardName = cardName_;
			this.cardImage = cardImage_;
			this.status = status_;
		}

		public Card()
		{
		}
	}
}
